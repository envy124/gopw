package config

import (
	"github.com/stretchr/testify/assert"
	"log"
	"testing"
)

const serverAddr = "192.168.0.107:29000"

func TestLoad(t *testing.T) {
	c := Load()
	t.Log(c.Servers)
	for _, v := range c.Servers {
		t.Log(v.Addr)
		for _, acc := range v.Accounts {
			assert.NotNil(t, acc.Login)
		}
	}
}

func TestAddServer(t *testing.T) {
	c := Load()
	c.AddServer("127.0.0.1")
	c.Dump()
	newConfig := Load()
	_, ok := newConfig.Servers["127.0.0.1"]
	assert.True(t, ok)
}

func TestConnect(t *testing.T) {
	c := Load()
	for client := range c.GetClients(serverAddr) {
		packet := client.Recv()
		log.Print(packet)
		client.Close()
	}
}
