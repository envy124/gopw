package config

import (
	"encoding/json"
	"gopw/net/client"
	"gopw/util"
	"io/ioutil"
	"os"
	"path"
)

const name = "pw.json"

type Account struct {
	Banned      bool   `json:"banned"`
	DefaultRole uint32 `json:"default_role"`
	Login       string `json:"login"`
	Passw       string `json:"passw"`
}

type Server struct {
	Addr           string              `json:"addr"`
	BlackList      []string            `json:"black_list"`
	DefaultAccount string              `json:"default_account"`
	MasterName     []string            `json:"master_name"`
	TriggerFoe     []uint32            `json:"trigger_foe"`
	TriggerFriend  []uint32            `json:"trigger_friend"`
	WhiteList      []string            `json:"white_list"`
	Accounts       map[string]*Account `json:"account"`
}

type Config struct {
	Servers  map[string]*Server `json:"server"`
	location string
}

func New() *Config {
	return &Config{
		make(map[string]*Server),
		path.Join(os.Getenv("GOPATH"), "src", "gopw", "config", name),
	}
}

func Load() *Config {
	config := New()
	data, err := ioutil.ReadFile(config.location)
	if err != nil {
		config.Dump()
		return config
	}
	err = json.Unmarshal(data, config)
	util.PanicIf(err)
	return config
}

func (c *Config) Dump() {
	bytes, err := json.MarshalIndent(c, "", "    ")
	util.PanicIf(err)
	ioutil.WriteFile(c.location, bytes, 0666)
}

func (c *Config) AddServer(addr string) {
	c.Servers[addr] = &Server{Addr: addr, Accounts: make(map[string]*Account)}
}

func (c *Config) GetClients(serverAddr string) chan *client.Client {
	output := make(chan *client.Client, len(c.Servers[serverAddr].Accounts))
	go func() {
		for _, acc := range c.Servers[serverAddr].Accounts {
			c := client.NewClient(acc.Login)
			c.Connect(serverAddr)
			output <- c
		}
		close(output)
	}()
	return output
}
