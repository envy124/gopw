package proxy

import (
	"fmt"
	"gopw/ds"
	"gopw/net/packet"
	"hash/crc32"
	"net"
)

type ProxyClient struct {
	clientSocket, serverSocket net.Conn
	mitm                       Hijack
	isWork                     bool
	sender                     chan<- packet.Serializer
}

func newClient(cs, ss net.Conn) *ProxyClient {
	p := &ProxyClient{clientSocket: cs, serverSocket: ss}
	fmt.Printf("%v -> %v\n", cs.RemoteAddr(), ss.RemoteAddr())
	p.sender = p.send()
	p.isWork = true
	p.recv(p.serverSocket, true)
	p.recv(p.clientSocket, false)
	return p
}

func (p *ProxyClient) Close() {
	if p.isWork {
		close(p.sender)
		p.clientSocket.Close()
		p.serverSocket.Close()
		p.mitm.client.Close()
		p.mitm.server.Close()
		p.isWork = false
	}
}

func (p *ProxyClient) handlePacket(_packet *packet.Packet) {
	if !isForbidden(_packet) {
		if !p.mitm.authDone {
			p.mitm.handlePacket(_packet)
		}
	}
}

func (p *ProxyClient) handleContainer(c *Container) {
	c.filterForbidden()
}

func (p *ProxyClient) recv(socket net.Conn, isServer bool) {
	var decoder func([]byte) []byte
	var deserializer func(*ds.DataStream) chan []*packet.Packet
	if isServer {
		decoder = p.mitm.DecodeServer
		deserializer = deserializeServer
	} else {
		decoder = p.mitm.DecodeClient
		deserializer = deserializeClient
	}
	go func() {
		stream := new(ds.DataStream)
		buffer := make([]byte, BUFFER_SIZE)
		for p.isWork {
			socket.SetReadDeadline(deadline())
			count, err := socket.Read(buffer)
			fmt.Println("recvraw", count)
			if err != nil {
				break
			}
			stream.WriteBytes(decoder(buffer[:count]))
			if isServer {
				fmt.Printf("   recv s2c %v %v %x\n", stream, stream.Count(), crc32.ChecksumIEEE(stream.GetBytes()))
			} else {
				fmt.Printf("   recv c2s %v %v %x\n", stream, stream.Count(), crc32.ChecksumIEEE(stream.GetBytes()))
			}
			for _packet := range deserializer(stream) {
				if len(_packet) == 1 {
					p.handlePacket(_packet[0])
					if _packet[0].Type == packet.ServerContainer {
						p.sender <- &Container{packets: _packet}
					} else {
						p.sender <- _packet[0]
					}
				} else {
					container := &Container{packets: _packet}
					container.filterForbidden()
					p.sender <- container
				}
			}
			stream.Reset()
		}
		p.Close()
	}()
}

func (p *ProxyClient) sendTest(_packet packet.Serializer) {
	var err error
	// fmt.Println("send", _packet)
	data := p.mitm.EncodePacket(_packet)
	// fmt.Println("sendraw", len(data))
	switch _packet.ByteType() {
	case packet.ClientPacket, packet.ClientContainer:
		p.serverSocket.SetWriteDeadline(deadline())
		_, err = p.serverSocket.Write(data)
	case packet.ServerPacket, packet.ServerContainer:
		p.clientSocket.SetWriteDeadline(deadline())
		_, err = p.clientSocket.Write(data)
	default:
		panic(UnknownPacketErr)
	}
	if err != nil || !p.isWork {
		p.Close()
	}
}

func (p *ProxyClient) send() chan<- packet.Serializer {
	sendChan := make(chan packet.Serializer)
	go func() {
		var err error
		for _packet := range sendChan {
			data := p.mitm.EncodePacket(_packet)
			switch _packet.ByteType() {
			case packet.ClientPacket, packet.ClientContainer:
				p.serverSocket.SetWriteDeadline(deadline())
				_, err = p.serverSocket.Write(data)
			case packet.ServerPacket, packet.ServerContainer:
				p.clientSocket.SetWriteDeadline(deadline())
				_, err = p.clientSocket.Write(data)
			default:
				panic(UnknownPacketErr)
			}
			if err != nil || !p.isWork {
				break
			}
		}
	}()
	return sendChan
}
