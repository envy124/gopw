package proxy

import (
	"errors"
	"fmt"
	"gopw/ds"
	"gopw/net/client/crypt"
	"gopw/net/packet"
	"time"
)

const (
	SERVER_HELLO = 1
	CLIENT_LOGIN = 2
	SMKEY        = 3
	CMKEY        = 3
)

var UnknownPacketErr error = errors.New("Unknown packet type passed to Hijack.handlePacket")

type Hijack struct {
	clientHash []byte
	authDone   bool
	login      string
	server     crypt.EncDec
	client     crypt.EncDecServer
}

func (h *Hijack) handleServer(p *packet.Packet) {
	newBody := new(ds.DataStream)
	switch p.Id {
	case SERVER_HELLO:
		hello := p.Body.ReadData(false)
		time.Sleep(5 * time.Millisecond)
		helloPacket(newBody, hello, VERSION)
	case SMKEY:
		smkey := p.Body.ReadData(false)
		keyPacket(newBody, smkey, p.Body.ReadBool())
		decode := crypt.GetHmac([]byte(h.login), append(h.clientHash, smkey...))
		h.client.CreateDecoder(decode)
		h.server.CreateEncoder(decode)
	}
	p.Body = newBody
}

func (h *Hijack) handleClient(p *packet.Packet) {
	newBody := new(ds.DataStream)
	switch p.Id {
	case CLIENT_LOGIN:
		h.login = p.Body.ReadAString()
		fmt.Println(h.login, "trying to login")
		h.clientHash = p.Body.ReadData(false)
		loginPacket(newBody, h.login, h.clientHash)
	case CMKEY:
		cmkey := p.Body.ReadData(false)
		keyPacket(newBody, cmkey, p.Body.ReadBool())
		encode := crypt.GetHmac([]byte(h.login), append(h.clientHash, cmkey...))
		h.client.CreateEncoder(encode)
		h.server.CreateDecoder(encode)
		h.authDone = true
	}
	p.Body = newBody
}

func (h *Hijack) handlePacket(p *packet.Packet) {
	switch p.Type {
	case packet.ServerPacket:
		h.handleServer(p)
	case packet.ClientPacket:
		h.handleClient(p)
	default:
		panic(UnknownPacketErr)
	}
}

func (h *Hijack) EncodePacket(p packet.Serializer) []byte {
	switch p.ByteType() {
	case packet.ClientPacket, packet.ClientContainer:
		return h.server.Encode(p)
	case packet.ServerPacket, packet.ServerContainer:
		return h.client.Encode(p)
	}
	panic(UnknownPacketErr)
}

func (h *Hijack) DecodeClient(data []byte) []byte {
	return h.client.Decode(data)
}

func (h *Hijack) DecodeServer(data []byte) []byte {
	return h.server.Decode(data)
}
