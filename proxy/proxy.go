package proxy

import (
	"fmt"
	"gopw/util"
	"gotools/netHelp"
	"gotools/stack"
	"net"
	"sync"
	"time"
)

const (
	PROXY_ADDR       = "192.168.0.103:29000"
	PW_ADDR          = "192.168.0.107:29000"
	BUFFER_SIZE      = 0x1000
	TIMEOUT          = 20 * time.Second
	SHUTDOWN_TIMEOUT = 200 * time.Second
	VERSION          = "134"
)

func deadline() time.Time {
	return time.Now().Add(TIMEOUT)
}

type Server struct {
	addr, pwAddr string
	socket       net.Listener
	isWork       bool
	clients      stack.Stack
	wg           sync.WaitGroup
}

func CreateDefault() *Server {
	s := &Server{}
	s.addr = PROXY_ADDR
	s.pwAddr = PW_ADDR
	return s
}

func Create(proxyAddr, pwAddr string) *Server {
	s := &Server{}
	s.addr = proxyAddr
	s.pwAddr = pwAddr
	return s
}

func (s *Server) Close() {
	if s.isWork {
		s.socket.Close()
		for {
			client, err := s.clients.Pop()
			if err != nil {
				break
			}
			(client.(*ProxyClient)).Close()
		}
		s.wg.Done()
		s.isWork = false
	}
}

func (s *Server) shutdownIfNoClients() {
	go func() {
		closeAttempt := false
		for _ = range time.Tick(5 * time.Second) {
			haveOnlineClients := false
			s.clients.ForEach(func(c interface{}) {
				client := c.(*ProxyClient)
				if client.isWork {
					haveOnlineClients = true
				}
			})
			if !haveOnlineClients {
				if closeAttempt {
					break
				}
				closeAttempt = true
				time.Sleep(SHUTDOWN_TIMEOUT)
			}
		}
		s.Close()
	}()
}

func (s *Server) Listen() {
	s.wg.Add(1)
	s.shutdownIfNoClients()
	go func() {
		socket, err := net.Listen("tcp", s.addr)
		util.PanicIf(err)
		fmt.Println("Server started at", socket.Addr())
		s.socket = socket
		s.isWork = true
		for s.isWork {
			client, err := s.socket.Accept()
			if err != nil {
				fmt.Println(err)
				break
			}
			pwserver, err := netHelp.Connect(s.pwAddr)
			util.PanicIf(err)
			s.clients.Push(newClient(client, pwserver))
		}
		s.Close()
	}()
}

func (s *Server) Join() {
	s.wg.Wait()
}
