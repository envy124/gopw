package proxy

import (
	"testing"
)

func TestServer(t *testing.T) {
	s := CreateDefault()
	defer s.Close()
	s.Listen()
	s.Join()
}
