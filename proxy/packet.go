package proxy

import (
	"gopw/ds"
	"gopw/net/packet"
)

var forbiddenPackets = []uint32{
	0x138B, 0x1389, 0x13a8, 0x13a0,
	0x138a, 0x13a2, 0x13a7, 0x138c,
	0x138d, 0x13a4, 0x1392, 0x1398,
	0x1395, 0x1390, 0x139e, 0x139f,
	0x13a1, 0x139c, 0x1396, 0x138f,
	0x1393, 0x139a, 0x139b, 0x1394,
	0x13a5,
}

type Container struct {
	Type    byte
	packets []*packet.Packet
}

func helloPacket(d *ds.DataStream, hello []byte, version string) {
	d.Write(hello)
	d.WriteByte(0)
	d.WriteBytes([]byte(version))
	d.WriteByte(0)
}

func loginPacket(d *ds.DataStream, login string, clientHash []byte) {
	d.WriteAString(login)
	d.Write(clientHash)
}

func keyPacket(d *ds.DataStream, key []byte, forse bool) {
	d.Write(key)
	d.WriteBool(forse)
}

func isForbidden(_packet *packet.Packet) bool {
	for _, forbid := range forbiddenPackets {
		if _packet.Id == forbid {
			return true
		}
	}
	return false
}

func deserializeServer(stream *ds.DataStream) chan []*packet.Packet {
	output := make(chan []*packet.Packet)
	go func() {
		for {
			id, err := stream.TryReadCuint()
			if err != nil {
				break
			}
			length, err := stream.TryReadCuint()
			if err != nil {
				break
			}
			if !stream.CanRead(length) {
				break
			}
			container := ds.FromBytes(stream.ReadBytes(length))
			container.SetSwaped(true)
			if id != 0 {
				output <- []*packet.Packet{
					&packet.Packet{Type: packet.ServerPacket, Id: id, Body: container}}
			} else if id == 0 {
				container.SetSwaped(false)
				var containerStorage []*packet.Packet
				for container.CanRead(1) {
					// fmt.Println(container)
					containerId := container.ReadCuint()
					container.ReadCuint()
					containerLen := container.ReadCuint() - 2
					containerType := container.ReadWord(nil)
					body := ds.FromBytes(container.ReadBytes(containerLen))
					containerStorage = append(containerStorage, &packet.Packet{packet.ServerContainer, uint32(containerType), containerId, body})
				}
				output <- containerStorage
			}
			stream.Flush()
		}
		close(output)
	}()
	return output
}

func deserializeClient(stream *ds.DataStream) chan []*packet.Packet {
	output := make(chan []*packet.Packet)
	go func() {
		for {
			id, err := stream.TryReadCuint()
			if err != nil {
				break
			}
			length, err := stream.TryReadCuint()
			if err != nil {
				break
			}
			if !stream.CanRead(length) {
				break
			}
			switch id {
			case 0x22:
				length = stream.ReadCuint() - 2
				containerId := stream.ReadWord(nil)
				body := ds.FromBytes(stream.ReadBytes(length))
				output <- []*packet.Packet{
					&packet.Packet{packet.ClientContainer, uint32(containerId), id, body}}
			default:
				body := ds.FromBytes(stream.ReadBytes(length))
				output <- []*packet.Packet{
					&packet.Packet{Type: packet.ClientPacket, Id: id, Body: body}}
			}
			stream.Flush()
		}
		close(output)
	}()
	return output
}

func (c *Container) ByteType() byte {
	return c.Type
}

func (c *Container) Append(p *packet.Packet) {
	c.packets = append(c.packets, p)
}

func (c *Container) filterForbidden() {
	var goodPackets []*packet.Packet
	for _, _packet := range c.packets {
		if !isForbidden(_packet) {
			goodPackets = append(goodPackets, _packet)
		}
	}
	c.packets = goodPackets
}

func (c *Container) Serialize(id *uint32) []byte {
	buffer := new(ds.DataStream)
	for _, p := range c.packets {
		buffer.WriteBytes(p.Serialize(nil))
	}
	output := new(ds.DataStream)
	output.WriteByte(0)
	output.WriteCuint(buffer.Count())
	output.WriteBytes(buffer.GetBytes())
	return output.GetBytes()
}
