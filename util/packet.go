package util

import (
	"bufio"
	"os"
	"path"
	"path/filepath"
	"strings"
)

var testDataDir = path.Join(os.Getenv("GOPATH"), "src", "gopw", "test_data")

func LoadFromFile(name, filter string) chan string {
	return loadRelativeFile(path.Join(testDataDir, name), filter)
}

func loadRelativeFile(name, filter string) chan string {
	f, err := os.Open(name)
	PanicIf(err)
	output := make(chan string)
	go func() {
		defer f.Close()
		reader := bufio.NewReader(f)
		for {
			bytes, _, err := reader.ReadLine()
			if err != nil {
				break
			}
			line := string(bytes)
			if filter == "" {
				output <- line
			} else if strings.Contains(line, filter) {
				parts := strings.Split(line, " ")
				output <- parts[len(parts)-1]
			}
		}
		close(output)
	}()
	return output
}

func LoadFromDir(dir, fileFilter, filter string) chan string {
	output := make(chan string)
	if fileFilter == "" {
		fileFilter = "*"
	}
	files, err := filepath.Glob(path.Join(testDataDir, dir, fileFilter))
	PanicIf(err)
	go func() {
		for _, file := range files {
			for line := range loadRelativeFile(file, filter) {
				output <- line
			}
		}
		close(output)
	}()
	return output
}

func LoadSliceFromDir(dir, fileFilter, filter string) []string {
	var output []string
	if fileFilter == "" {
		fileFilter = "*"
	}
	files, err := filepath.Glob(path.Join(testDataDir, dir, fileFilter))
	PanicIf(err)
	for _, file := range files {
		for line := range loadRelativeFile(file, filter) {
			output = append(output, line)
		}
	}
	return output
}
