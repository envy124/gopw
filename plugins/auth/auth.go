package auth

import (
	"crypto/rand"
	"fmt"
	"gopw/config"
	pwclient "gopw/net/client"
	"gopw/net/client/crypt"
	"gopw/net/packet"
	"gopw/net/protocol"
	"gopw/net/protocol/client"
	"gopw/net/protocol/server"
	"sync"
)

const (
	ok = 0 + iota
	authFail
	banned
	authDone
)

var authWait sync.WaitGroup

type AuthPlugin struct {
	conn   *pwclient.Client
	server *config.Server
	data   *protocol.LogonData
}

func ReserveClients(clients int) {
	authWait.Add(clients)
}

func Join() {
	authWait.Wait()
}

func New(serverAddr string, c *pwclient.Client) *AuthPlugin {
	return &AuthPlugin{c,
		config.Load().Servers[serverAddr],
		new(protocol.LogonData),
	}
}

func (a *AuthPlugin) loadCredentials() *config.Account {
	account := a.server.Accounts[a.conn.Login()]
	a.data.Login = account.Login
	a.data.Passw = account.Passw
	a.data.DefaultRoleIndex = account.DefaultRole
	a.data.Force = true
	return account
}

func (a *AuthPlugin) handle(p *packet.Packet) (*packet.Packet, int) {
	var handlerId uint32
	var status int
	fmt.Println(p)
	protocol.HandleLogonPacket(a.data, p)
	switch p.Id {
	case server.Hello:
		handlerId = client.Login
		a.data.Hash = crypt.GetHash(a.data.HelloKey, a.data.Login+a.data.Passw)
	case server.AuthFailure:
		status = authFail
		fmt.Println(a.data.FailMsg, a.data.Login, a.data.Passw)
	case server.Smkey:
		handlerId = client.Cmkey
		a.data.Cmkey = make([]byte, 0x10)
		rand.Read(a.data.Cmkey)
		a.conn.EncDec().CreateEncoder(
			crypt.GetHmac(
				[]byte(a.data.Login),
				append(a.data.Hash, a.data.Smkey...)))
		a.conn.EncDec().CreateDecoder(
			crypt.GetHmac(
				[]byte(a.data.Login),
				append(a.data.Hash, a.data.Cmkey...)))
	case server.OnlineAnnounce:
		handlerId = client.RequestRoleSlot
	case server.RoleSlotRe:
		if a.data.NextSlot > 8 || a.data.NextSlot == -1 {
			handlerId = client.SelectRole
		} else {
			handlerId = client.RequestRoleSlot
		}
	case server.SelectAccepted:
		if a.data.SelectSuccess {
			handlerId = client.EnterWorld
			status = authDone
		}
	default:
		panic(fmt.Errorf("AuthPlugin unknown packet %v", p))
	}
	stub := &packet.Packet{Type: packet.ClientPacket, Id: handlerId}
	protocol.BuildLogonPacket(a.data, stub)
	return stub, status
}

func (a *AuthPlugin) Activate() {
	a.data.NextSlot = -1
	if a.loadCredentials().Banned {
		fmt.Println("Account", a.data.Login, "is banned, skipping...")
		return
	}
	go func() {
		for a.conn.IsWork() {
			if answer, status := a.handle(a.conn.Recv()); status == authDone {
				a.conn.Send(answer)
				break
			} else if status == authFail {
				fmt.Println("Account", a.data.Login, a.data.FailMsg)
			} else if status == ok {
				a.conn.Send(answer)
			}
		}
		authWait.Done()
	}()
}
