package auth

import (
	"gopw/net/client"
	"testing"
)

func TestAuth(t *testing.T) {
	const addr = "192.168.0.107:29000"
	conn := client.NewClient("root")
	conn.Connect(addr)
	defer conn.Close()
	ReserveClients(1)
	auth := New(addr, conn)
	auth.Activate()
	Join()
}
