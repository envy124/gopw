package ds

import (
	"fmt"
	"reflect"
	"strings"
	"time"
)

func (ds *DataStream) Write(x interface{}) {
	switch data := x.(type) {
	case uint32:
		ds.WriteDword(data, nil)
	case int:
		ds.WriteDword(uint32(data), nil)
	case float32:
		ds.WriteFloat32(data)
	case uint16:
		ds.WriteWord(data, nil)
	case uint8:
		ds.WriteByte(data)
	case string:
		ds.WriteAString(data)
	case WString:
		ds.WriteWString(string(data))
	case []byte:
		ds.WriteCuint(uint32(len(data)))
		ds.WriteBytes(data)
	case bool:
		ds.WriteBool(data)
	case time.Time:
		ds.WriteTime(data)

	case *uint32:
		ds.WriteDword(*data, nil)
	case *int:
		ds.WriteDword(uint32(*data), nil)
	case *float32:
		ds.WriteFloat32(*data)
	case *uint16:
		ds.WriteWord(*data, nil)
	case *uint8:
		ds.WriteByte(*data)
	case *string:
		ds.WriteAString(*data)
	case *WString:
		ds.WriteWString(string(*data))
	case *bool:
		ds.WriteBool(*data)
	case *[]uint8:
		ds.WriteCuint(uint32(len(*data)))
		ds.WriteBytes(*data)

	// case *Uint32Packed:
	// 	ds.WriteDword(uint32(*data) * 100)
	// case *Uint32s:
	// 	ds.swap = true
	// 	defer ds.Swap(false)
	// 	ds.WriteDword(uint32(*data))
	// case *CUint:
	// 	ds.PackLength()
	case *time.Time:
		ds.WriteTime(*data)

	default:
		// ds.trySerialize(data)
	}
}

func (ds *DataStream) Read(param interface{}) {
	switch data := param.(type) {
	case *uint32:
		*data = ds.ReadDword(nil)
	case *float32:
		*data = ds.ReadFloat32()
	case *uint16:
		*data = ds.ReadWord(nil)
	case *uint8:
		*data = ds.ReadByte()
	case *string:
		*data = ds.ReadAString()
	case *WString:
		*data = WString(ds.ReadWString())
	case *[]byte:
		*data = ds.ReadBytes(ds.ReadCuint())
	case *bool:
		*data = ds.ReadBool()

	// case *Uint32Packed:
	// 	*data = Uint32Packed(ds.ReadDword() / 100)
	// case *Uint16Packed:
	// 	*data = Uint16Packed(ds.ReadWord() / 100)
	// case *Float32Packed:
	// 	*data = Float32Packed(ds.ReadFloat32() / 100)

	case *time.Time:
		*data = ds.ReadTime()
	default:
		ds.Deserialize(data)
		// panic("AutoMarshall Unknown autoRead type!" + fmt.Sprintf("%T", data))
		// fmt.Printf("AutoMarshall::TryDeSerialize %T\n", data)
		// ds.tryDeSerialize(data)
	}
}

func (ds *DataStream) Deserialize(object interface{}) {
	container := reflect.ValueOf(object).Elem()
	switch container.Kind() {
	case reflect.Array, reflect.Slice:
		for i := 0; i < container.Len(); i++ {
			ds.Read(container.Index(i).Addr().Interface())
		}
	case reflect.Struct:
		typeOf := container.Type()
		for i := 0; i < container.NumField(); i++ {
			field := container.Field(i)
			if mod, ok := typeOf.Field(i).Tag.Lookup("ds"); ok {
				if strings.Contains(mod, "nested") {
					var length uint32
					if strings.Contains(mod, "int32") {
						length = ds.ReadDword(nil)
					} else {
						length = ds.ReadCuint()
					}
					field.Set(reflect.MakeSlice(typeOf.Field(i).Type, int(length), int(length)))
				}
			}
			ds.Read(field.Addr().Interface())
		}
	default:
		panic(fmt.Errorf("Can't deserialize %v", container))
	}
}
