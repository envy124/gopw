package ds

import (
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"math"
	"time"
)

const displayHeaderLen = 32

var CantRead error = errors.New("CantRead")
var FromStringErr error = errors.New("Can't init DataStream from this string")
var SwapArgErr error = errors.New("Swap argument must be nil or bool")

type DataStream struct {
	buffer []byte
	swaped bool
	count  uint32
	pos    uint32
}

func (ds *DataStream) String() string {
	if ds.count > displayHeaderLen {
		return fmt.Sprintf("%X", ds.buffer[ds.pos:ds.pos+displayHeaderLen])
	}
	return fmt.Sprintf("%X", ds.buffer[ds.pos:ds.pos+ds.count])
}

func (ds *DataStream) Count() uint32 {
	return ds.count
}

func (ds *DataStream) GetBytes() []byte {
	return ds.buffer[ds.pos:ds.count]
}

func (ds *DataStream) reserve(count uint32) {
	if len(ds.buffer) == 0 {
		ds.buffer = make([]byte, count)
	}
	if count > uint32(len(ds.buffer)) {
		ds.buffer = append(ds.buffer, make([]byte, count)...)
	}
}

func (ds *DataStream) CanRead(length uint32) bool {
	return ds.pos+length <= ds.count
}

func (ds *DataStream) Skip(count uint32) {
	if ds.pos+count > ds.count {
		panic("Skip: pos + count > ds.count")
	}
	ds.pos += count
}

func (ds *DataStream) Flush() {
	if ds.pos == 0 {
		return
	}
	length := ds.count - ds.pos
	copy(ds.buffer[:length], ds.buffer[ds.pos:ds.pos+length])
	ds.pos = 0
	ds.count = length
}

func (ds *DataStream) Reset() {
	ds.pos = 0
}

func (ds *DataStream) Clear() {
	ds.pos = 0
	ds.count = 0
}

func (d *DataStream) SetSwaped(x bool) {
	d.swaped = x
}

func (ds *DataStream) WriteBytes(data []byte) {
	ds.reserve(ds.count + uint32(len(data)))
	for _, bt := range data {
		ds.buffer[ds.count] = bt
		ds.count++
	}
}

func (ds *DataStream) ReadBytes(count uint32) []byte {
	if ds.pos+count > ds.count {
		panic("ReadBytes: pos + count > ds.count")
	}
	output := make([]byte, count)
	for i, _ := range output {
		output[i] = ds.buffer[ds.pos]
		ds.pos++
	}
	return output
}

func (ds *DataStream) ReadByte() byte {
	if ds.pos >= ds.count {
		panic("ReadByte: ds.pos > ds.count")
	}
	output := ds.buffer[ds.pos]
	ds.pos++
	return output
}

func (ds *DataStream) ReadBool() bool {
	if ds.ReadByte() == 1 {
		return true
	} else {
		return false
	}
}

func (ds *DataStream) ReadDword(swap interface{}) uint32 {
	bytes := ds.ReadBytes(4)
	if value, ok := swap.(bool); (ok && value) || (!ok && ds.swaped) {
		return binary.BigEndian.Uint32(bytes)
	} else if (ok && !value) || !(ok && ds.swaped) {
		return binary.LittleEndian.Uint32(bytes)
	}
	panic(SwapArgErr)
}

func (ds *DataStream) ReadWord(swap interface{}) uint16 {
	bytes := ds.ReadBytes(2)
	if value, ok := swap.(bool); (ok && value) || (!ok && ds.swaped) {
		return binary.BigEndian.Uint16(bytes)
	} else if (ok && !value) || !(ok && ds.swaped) {
		return binary.LittleEndian.Uint16(bytes)
	}
	panic(SwapArgErr)
}

func (ds *DataStream) ReadFloat32() float32 {
	return math.Float32frombits(ds.ReadDword(nil))
}

func (ds *DataStream) ReadTime() time.Time {
	return time.Unix(int64(ds.ReadDword(nil)), 0)
}

func (ds *DataStream) TryReadCuint() (uint32, error) {
	if !ds.CanRead(1) {
		return 0, CantRead
	}
	firstByte := ds.buffer[ds.pos] & 0xe0
	switch firstByte {
	case 0xe0:
		if !ds.CanRead(5) {
			return 0, CantRead
		}
		ds.ReadByte()
		return ds.ReadDword(true), nil
	case 0xc0:
		if !ds.CanRead(4) {
			return 0, CantRead
		}
		return ds.ReadDword(true) & 0x3fffffff, nil
	case 0x80, 0xa0:
		if !ds.CanRead(2) {
			return 0, CantRead
		}
		return uint32(ds.ReadWord(true) & 0x3fff), nil
	default:
		return uint32(ds.ReadByte()), nil
	}
}

func (ds *DataStream) ReadCuint() uint32 {
	if !ds.CanRead(1) {
		panic("ReadCuint: CantRead")
	}
	firstByte := ds.buffer[ds.pos] & 0xe0
	switch firstByte {
	case 0xe0:
		ds.ReadByte()
		return uint32(ds.ReadDword(true))
	case 0xc0:
		return uint32(ds.ReadDword(true)) & 0x3fffffff
	case 0x80, 0xa0:
		return uint32(ds.ReadWord(true)) & 0x3fff
	default:
		return uint32(ds.ReadByte())
	}
}

func (ds *DataStream) ReadData(lenInt32 bool) []byte {
	var length uint32
	if lenInt32 {
		length = ds.ReadDword(nil)
	} else {
		length = ds.ReadCuint()
	}
	return ds.ReadBytes(length)
}

func (ds *DataStream) ReadDS() *DataStream {
	return nil
}

func (ds *DataStream) ReadAString() string {
	return string(ds.ReadBytes(ds.ReadCuint()))
}

func (ds *DataStream) ReadWString() string {
	return WString("").decodeUTF16(ds.ReadBytes(ds.ReadCuint()))
}

func (ds *DataStream) ReadContainer() {

}

func (ds *DataStream) WriteByte(x byte) {
	ds.reserve(ds.count + 1)
	ds.buffer[ds.count] = x
	ds.count++
}

func (ds *DataStream) WriteBool(x bool) {
	if x {
		ds.WriteByte(1)
	} else {
		ds.WriteByte(0)
	}
}

func (ds *DataStream) WriteDword(x uint32, swap interface{}) {
	bytes := make([]byte, 4)
	if value, ok := swap.(bool); (ok && value) || (!ok && ds.swaped) {
		binary.BigEndian.PutUint32(bytes, x)
	} else if (ok && !value) || !(ok && ds.swaped) {
		binary.LittleEndian.PutUint32(bytes, x)
	} else {
		panic(SwapArgErr)
	}
	ds.WriteBytes(bytes)
}

func (ds *DataStream) WriteWord(x uint16, swap interface{}) {
	bytes := make([]byte, 2)
	if value, ok := swap.(bool); (ok && value) || (!ok && ds.swaped) {
		binary.BigEndian.PutUint16(bytes, x)
	} else if (ok && !value) || !(ok && ds.swaped) {
		binary.LittleEndian.PutUint16(bytes, x)
	} else {
		panic(SwapArgErr)
	}
	ds.WriteBytes(bytes)
}

func (ds *DataStream) WriteFloat32(x float32) {
	ds.WriteDword(math.Float32bits(x), nil)
}

func (ds *DataStream) WriteTime(x time.Time) {
	ds.WriteDword(uint32(x.Unix()), nil)
}

func (ds *DataStream) WriteAString(x string) {
	ds.WriteCuint(uint32(len(x)))
	ds.WriteBytes([]byte(x))
}

func (ds *DataStream) WriteWString(x string) {
	wstr := WString(x).encodeUTF16()
	ds.WriteCuint(uint32(len(wstr)))
	ds.WriteBytes(wstr)
}

func (ds *DataStream) WriteCuint(x uint32) {
	if x < 0x80 {
		ds.WriteByte(byte(x))
	} else if x < 0x4000 {
		ds.WriteWord(uint16(x|0x8000), true)
	} else if x < 0x20000000 {
		ds.WriteDword(x|0xC0000000, true)
	} else {
		ds.WriteByte(0xe0)
		ds.WriteDword(x, true)
	}
}

func FromString(hexstr string) *DataStream {
	if bytes, err := hex.DecodeString(hexstr); err != nil {
		panic(FromStringErr)
	} else {
		return FromBytes(bytes)
	}
}

func FromBytes(bytes []byte) *DataStream {
	ds := new(DataStream)
	ds.WriteBytes(bytes)
	return ds
}
