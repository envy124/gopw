package ds

import (
	"bytes"
	"fmt"
	"unicode/utf16"
	"unicode/utf8"
)

/**
 * Unicode string, 2 bytes per char
 */
type WString string

func (wstr WString) Len() int {
	return len(wstr.encodeUTF16()) / 2
}

func (data WString) encodeUTF16() []byte {
	u8 := &bytes.Buffer{}
	for _, ch := range data {
		u8.WriteByte(uint8(ch))
		u8.WriteByte(uint8(uint16(ch) >> 8))
	}
	return u8.Bytes()
}

func (wstr WString) decodeUTF16(b []byte) string {
	if len(b)%2 != 0 {
		panic(fmt.Errorf("decodeUTF16 wrong slice %v", b))
	}

	u16s := make([]uint16, 1)

	ret := &bytes.Buffer{}

	b8buf := make([]byte, 4)

	lb := len(b)
	for i := 0; i < lb; i += 2 {
		u16s[0] = uint16(b[i]) + (uint16(b[i+1]) << 8)
		r := utf16.Decode(u16s)
		n := utf8.EncodeRune(b8buf, r[0])
		ret.Write(b8buf[:n])
	}
	return ret.String()
}
