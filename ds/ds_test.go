package ds

import (
	"github.com/stretchr/testify/assert"
	"gopw/util"
	"testing"
	"time"
)

var _ = time.Time{}

type Equip struct {
	Id           uint32
	CellId       uint32
	Count        uint32
	MaxCount     uint32
	Octet        []byte
	ProcType     uint32
	ExpireDate   time.Time
	Guid1, Guid2 uint32
	Mask         uint32
}

type Role struct {
	Id         uint32
	Gender     byte
	Race       byte
	Occupation byte
	Level      uint32
	Level2     uint32
	Name       WString
	Face       []byte
	Equip      []Equip `ds:"nested"`
	Available  bool
	DeleteTime time.Time
	CreateTime time.Time
	LastOnline time.Time
	Position   [3]float32
	WorldId    uint32
	Unk        uint16
}

func TestCuint(t *testing.T) {
	s := new(DataStream)
	var i uint32
	for i = 0; i < 0x2000; i++ {
		s.WriteCuint(i)
		assert.Equal(t, uint32(i), s.ReadCuint())
		// s.Reset()
	}
}

func TestDword(t *testing.T) {
	const dword uint32 = 150
	s := new(DataStream)
	s.WriteDword(dword, nil)
	s.WriteDword(dword, true)
	s.WriteDword(dword, false)
	assert.Equal(t, dword, s.ReadDword(nil))
	assert.Equal(t, dword, s.ReadDword(true))
	assert.Equal(t, dword, s.ReadDword(false))
}

func TestWord(t *testing.T) {
	const word uint16 = 150
	s := new(DataStream)
	s.WriteWord(word, nil)
	s.WriteWord(word, true)
	s.WriteWord(word, false)
	assert.Equal(t, word, s.ReadWord(nil))
	assert.Equal(t, word, s.ReadWord(true))
	assert.Equal(t, word, s.ReadWord(false))
}

func TestRoleRead(t *testing.T) {
	for _, hexstr := range util.LoadSliceFromDir("", "", "S2C")[3:6] {
		stream := FromString(hexstr)
		stream.swaped = true
		stream.ReadCuint()
		stream.ReadCuint()
		stream.Skip(17)
		role := new(Role)
		stream.Deserialize(role)
		t.Log(stream.pos, stream.count)
		t.Log(role)
	}
}
