package main

import (
	"fmt"
	"gopw/ds"
	"gopw/net/client/crypt/mppc"
	"gopw/proxy"
)

const serverAddr = "192.168.0.107:29000"

func main() {
	unpacker := mppc.NewUnpack()
	defer unpacker.Close()
	packet := ds.FromString("0419000000300000001600F0403E2002FFE0803C1780")
	fmt.Printf("%X\n", unpacker.Unpack(append(make([]byte, 0), packet.GetBytes()...)))
	server := proxy.Create("192.168.0.103:29000", serverAddr)
	server.Listen()
	server.Join()

}
