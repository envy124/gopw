package protocol

import (
	"fmt"
	"gopw/ds"
	"gopw/net/packet"
)

func HandleLogonPacket(storage *LogonData, p *packet.Packet) {
	if handler, ok := logonServer[p.Id]; ok {
		handler(storage, p.Body)
	} else {
		panic(fmt.Errorf("Logon can't handle %v", p))
	}
}

func BuildLogonPacket(storage *LogonData, stub *packet.Packet) {
	if builder, ok := logonClient[stub.Id]; ok {
		stub.Body = new(ds.DataStream)
		stub.Body.SetSwaped(true)
		builder(storage, stub.Body)
	} else {
		panic(fmt.Errorf("Logon can't build %v", stub))
	}
}

func (l *LogonData) hello(d *ds.DataStream) {
	l.HelloKey = d.ReadData(false)
	d.ReadByte()
	l.Version = string(d.ReadBytes(3))
	d.ReadByte()
}

func (l *LogonData) authSuccess(d *ds.DataStream) {
	l.Success = true
	l.Smkey = d.ReadData(false)
	d.ReadBool() //Forse
}

func (l *LogonData) onlineAnnounce(d *ds.DataStream) {
	l.AccountId = d.ReadDword(nil)
	d.ReadDword(nil) //localSid
}

func (l *LogonData) authFailure(d *ds.DataStream) {
	l.Success = false
	d.ReadByte()
	l.FailMsg = d.ReadAString()
}

func (l *LogonData) roleSlot(d *ds.DataStream) {
	d.Skip(4)
	l.NextSlot = int32(d.ReadDword(nil))
	d.Skip(8)
	if d.ReadBool() {
		// read char data
	}
}

func (l *LogonData) selectAccepted(d *ds.DataStream) {
	l.SelectSuccess = true
}

func (l *LogonData) createRoleRe(d *ds.DataStream) {

}

func (l *LogonData) login(d *ds.DataStream) {
	d.WriteAString(l.Login)
	d.Write(l.Hash)
}

func (l *LogonData) cmkey(d *ds.DataStream) {
	d.Write(l.Cmkey)
	d.WriteBool(l.Force)
}

func (l *LogonData) requestRoleSlot(d *ds.DataStream) {
	d.WriteDword(l.AccountId, nil)
	d.WriteDword(0, nil)
	d.WriteDword(uint32(l.NextSlot), nil)
}

func (l *LogonData) roleSelect(d *ds.DataStream) {
	d.WriteDword(l.DefaultRoleIndex+l.AccountId, nil)
}

func (l *LogonData) createRole(d *ds.DataStream) {

}

func (l *LogonData) enterWorld(d *ds.DataStream) {
	d.WriteDword(l.DefaultRoleIndex+l.AccountId, nil)
	d.WriteBytes(make([]byte, 8))
}
