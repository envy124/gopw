package protocol

import (
	"gopw/ds"
	"gopw/net/protocol/client"
	"gopw/net/protocol/server"
)

type logonProtocol map[uint32]func(*LogonData, *ds.DataStream)

var (
	logonServer logonProtocol
	logonClient logonProtocol
)

type LogonData struct {
	AccountId                    uint32
	Login, Passw                 string
	HelloKey, Smkey, Cmkey, Hash []byte
	Version                      string
	Success, Force               bool
	FailMsg                      string
	NextSlot                     int32
	SelectSuccess                bool
	DefaultRoleIndex             uint32
}

func init() {
	logonServer = logonProtocol{
		server.Hello:          (*LogonData).hello,
		server.Smkey:          (*LogonData).authSuccess,
		server.OnlineAnnounce: (*LogonData).onlineAnnounce,
		server.AuthFailure:    (*LogonData).authFailure,
		server.RoleSlotRe:     (*LogonData).roleSlot,
		server.SelectAccepted: (*LogonData).selectAccepted,
		server.CreateRoleRe:   (*LogonData).createRoleRe,
	}
	logonClient = logonProtocol{
		client.Login:           (*LogonData).login,
		client.Cmkey:           (*LogonData).cmkey,
		client.RequestRoleSlot: (*LogonData).requestRoleSlot,
		client.SelectRole:      (*LogonData).roleSelect,
		client.CreateRole:      (*LogonData).createRole,
		client.EnterWorld:      (*LogonData).enterWorld,
	}
}
