package protocol

import (
	"time"
)

type Equip struct {
	Id           uint32
	CellId       uint32
	Count        uint32
	MaxCount     uint32
	Octet        []byte
	ProcType     uint32
	ExpireDate   time.Time
	Guid1, Guid2 uint32
	Mask         uint32
}

type Role struct {
	Id         uint32
	Gender     byte
	Race       byte
	Occupation byte
	Level      uint32
	Level2     uint32
	Name       string
	Face       []byte
	Equip      []Equip `ds:"nested"`
	Available  bool
	DeleteTime time.Time
	CreateTime time.Time
	LastOnline time.Time
	Position   [3]float32
	WorldId    uint32
	Unk        uint16
}
