package packet

import (
	"fmt"
	"gopw/ds"
)

const (
	ServerPacket = 0 + iota
	ServerContainer
	ClientPacket
	ClientContainer
)

type Serializer interface {
	Serialize(id *uint32) []byte
	ByteType() byte
}

type Packet struct {
	Type          byte
	Id, Container uint32
	Body          *ds.DataStream
}

func (p *Packet) String() string {
	var shortcut string
	switch p.Type {
	case ServerPacket:
		shortcut = "S2CP"
	case ServerContainer:
		shortcut = "S2CC"
	case ClientPacket:
		shortcut = "C2SP"
	case ClientContainer:
		shortcut = "C2SC"
	}
	return fmt.Sprintf("%v Id %X Container %X %v", shortcut, p.Id, p.Container, p.Body)
}

func (p *Packet) ByteType() byte {
	return p.Type
}

func (p *Packet) SameType(packet *Packet) bool {
	return p.Type == packet.Type && p.Id == packet.Id && p.Container == packet.Container
}

func GetPackets(stream *ds.DataStream) <-chan *Packet {
	output := make(chan *Packet)
	go func() {
		for {
			id, err := stream.TryReadCuint()
			if err != nil {
				break
			}
			length, err := stream.TryReadCuint()
			if err != nil {
				break
			}
			if !stream.CanRead(length) {
				break
			}
			container := ds.FromBytes(stream.ReadBytes(length))
			container.SetSwaped(true)
			if id != 0 {
				output <- &Packet{Type: ServerPacket, Id: id, Body: container}
			} else if id == 0 {
				container.SetSwaped(false)
				for container.CanRead(1) {
					// fmt.Println(container)
					containerId := container.ReadCuint()
					container.ReadCuint()
					containerLen := container.ReadCuint() - 2
					containerType := container.ReadWord(nil)
					body := ds.FromBytes(container.ReadBytes(containerLen))
					output <- &Packet{ServerContainer, uint32(containerType), containerId, body}
				}
			}
			stream.Flush()
		}
		stream.Reset()
		close(output)
	}()
	return output
}

func createContainer(id uint32, body *ds.DataStream) *ds.DataStream {
	container := new(ds.DataStream)
	container.WriteCuint(body.Count() + 2)
	container.WriteWord(uint16(id), nil)
	container.WriteBytes(body.GetBytes())
	return container
}

func (p *Packet) Serialize(id *uint32) []byte {
	if id == nil {
		id = &p.Id
	}
	switch p.Type {
	case ServerPacket, ClientPacket:
		packet := new(ds.DataStream)
		packet.WriteCuint(*id)
		packet.WriteCuint(p.Body.Count())
		packet.WriteBytes(p.Body.GetBytes())
		return packet.GetBytes()
	case ClientContainer:
		p.Body = createContainer(p.Id, p.Body)
		p.Type = ClientPacket
		return p.Serialize(&p.Container)
	case ServerContainer:
		p.Body = createContainer(p.Id, p.Body)
		p.Type = ServerPacket
		return p.Serialize(&p.Container)
	}
	panic(fmt.Errorf("Can't Serialize %v\n", p))
}
