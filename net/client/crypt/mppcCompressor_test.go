package crypt

import (
	"github.com/stretchr/testify/assert"
	"gopw/ds"
	mppc "gopw/net/client/crypt/mppc_asm"
	"gopw/util"
	"testing"
)

func (x bitArray) String() string {
	var output string
	for _, bit := range x {
		if bit == 0 {
			output += "0"
		} else {
			output += "1"
		}
	}
	return output
}

func TestBinToByte(t *testing.T) {
	data := []byte{0xff, 0xff, 0xff, 0xff}
	buffer := byteToBit(data)
	assert.Equal(t, data, bitToByte(buffer))

	testData := util.LoadSliceFromDir("mppc", "", "S2C")
	data = ds.FromString(testData[1]).GetBytes()
	buffer = byteToBit(data)
	assert.Equal(t, data, bitToByte(buffer))
	// 5a015a+...
	bits := []byte{0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0}
	t.Log(bitToByte(bits))
}

func TestPackLittle(t *testing.T) {
	clean := []byte{0x5a, 1, 0x5a}
	mppcu := new(MppcUnpacker)
	assert.Equal(t, clean, mppcu.Unpack(MppcPack(clean)))
}

func TestPack(t *testing.T) {
	flag := false
	mppcu := new(MppcUnpacker)
	for _, hexstr := range util.LoadSliceFromDir("mppc", "", "S2C") {
		if !flag {
			flag = !flag
			continue
		}
		packet := ds.FromString(hexstr).GetBytes()
		// t.Logf("%X\n", packet)
		mppcPacked := MppcPack(packet)
		// t.Logf("%X\n", mppcPacked)
		assert.Equal(t, packet, mppcu.Unpack(mppcPacked))
	}
}

func TestUnpackAsm(t *testing.T) {
	unpacker := new(MppcUnpacker)
	compress := mppc.NewCompress()
	defer compress.Close()
	defer unpacker.Close()
	mppcPackedOrigin := ds.FromString("0419000000300000001600F0403E2002FFE0803C1780").GetBytes()
	unpackedOrigin := unpacker.Unpack(mppcPackedOrigin)
	packedMppcCWrapper := compress.Pack(unpackedOrigin)
	unpackedMppcCWrapper := unpacker.Unpack(packedMppcCWrapper)
	// t.Logf("%X\n", unpackedOrigin)
	t.Logf("%X\n", packedMppcCWrapper)
	// t.Logf("%X\n", unpackedMppcCWrapper)
	assert.Equal(t, unpackedOrigin, unpackedMppcCWrapper)
}

func TestCPackUnpack(t *testing.T) {
	compress := mppc.NewCompress()
	unpacker := new(MppcUnpacker)
	defer compress.Close()
	defer unpacker.Close()
	for i, hexstr := range util.LoadSliceFromDir("mppc", "", "S2C")[:1] {
		if i%2 == 0 {
			packet := ds.FromString(hexstr).GetBytes()
			mppcPacked := compress.Pack(packet)
			mppcUnpacked := unpacker.Unpack(mppcPacked)
			// t.Logf("%X\n", packet)
			// t.Logf("%X\n", mppcPacked)
			// t.Logf("%X\n", mppcUnpacked)
			assert.Equal(t, packet, mppcUnpacked)
		}
	}
}
