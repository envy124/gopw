package crypt

import (
	"crypto/rc4"
	"fmt"
	mppc "gopw/net/client/crypt/mppc_asm"
	"gopw/net/packet"
	"hash/crc32"
)

type EncDecServer struct {
	enc, dec *rc4.Cipher
	mppc     *mppc.MppcCompress
}

func (e *EncDecServer) Close() {
	if e.mppc != nil {
		e.mppc.Close()
	}
}

func (e *EncDecServer) CreateEncoder(key []byte) {
	var err error
	e.enc, err = rc4.NewCipher(key)
	if err != nil {
		panic(err)
	}
	e.mppc = mppc.NewCompress()
}

func (e *EncDecServer) CreateDecoder(key []byte) {
	var err error
	e.dec, err = rc4.NewCipher(key)
	if err != nil {
		panic(err)
	}
}

func (e *EncDecServer) Encode(p packet.Serializer) []byte {
	data := p.Serialize(nil)
	if e.enc == nil || e.mppc == nil {
		return data
	}
	fmt.Printf("sendraw s2c %X %v %x\n", data, len(data), crc32.ChecksumIEEE(data))
	output := e.mppc.Pack(data)
	e.enc.XORKeyStream(output, output)
	return output
}

func (e *EncDecServer) Decode(data []byte) []byte {
	if e.dec == nil {
		return data
	}
	output := make([]byte, len(data))
	e.dec.XORKeyStream(output, data)
	return output
}
