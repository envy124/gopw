package crypt

import (
	"crypto/hmac"
	"crypto/md5"
)

func GetHmac(key, data []byte) []byte {
	hash := hmac.New(md5.New, key)
	hash.Write(data)
	return hash.Sum(nil)
}

func GetHash(key []byte, logpass string) []byte {
	logPassMd5 := md5.New()
	logPassMd5.Write([]byte(logpass))
	return GetHmac(logPassMd5.Sum(nil), key)
}
