package crypt

import (
	"crypto/rc4"
	"fmt"
	"gopw/net/packet"
	"hash/crc32"
)

type EncDec struct {
	enc, dec *rc4.Cipher
	mppc     *MppcUnpacker
}

func (e *EncDec) Close() {
}

func (e *EncDec) CreateEncoder(key []byte) {
	var err error
	e.enc, err = rc4.NewCipher(key)
	if err != nil {
		panic(err)
	}
}

func (e *EncDec) CreateDecoder(key []byte) {
	var err error
	e.dec, err = rc4.NewCipher(key)
	if err != nil {
		panic(err)
	}
	e.mppc = new(MppcUnpacker)
}

func (e *EncDec) Encode(p packet.Serializer) []byte {
	data := p.Serialize(nil)
	if e.enc == nil {
		return data
	}
	fmt.Printf("sendraw c2s %X %v %x\n", data, len(data), crc32.ChecksumIEEE(data))
	output := make([]byte, len(data))
	e.enc.XORKeyStream(output, data)
	return output
}

func (e *EncDec) Decode(data []byte) []byte {
	if e.dec == nil || e.mppc == nil {
		return data
	}
	output := make([]byte, len(data))
	e.dec.XORKeyStream(output, data)
	return e.mppc.Unpack(output)
}
