package crypt

const byteLen int = 7

type bitArray []byte

func byteToBit(data []byte) bitArray {
	buffer := make([]byte, len(data)*8)
	for i := len(data) - 1; i >= 0; i-- {
		bt := data[i]
		for bitIndex := byteLen; bitIndex >= 0; bitIndex-- {
			buffer[i*8+bitIndex] = bt & 1
			bt >>= 1
		}
	}
	return buffer
}

func bitToByte(buffer []byte) []byte {
	for len(buffer)%8 != 0 {
		buffer = append(buffer, 0)
	}
	output := make([]byte, len(buffer)/8)
	for i, outputIndex := 0, 0; i < len(buffer); {
		for bitIndex := byteLen; bitIndex >= 0; bitIndex-- {
			output[outputIndex] |= buffer[i+bitIndex] << uint((byteLen)-bitIndex)
		}
		outputIndex++
		i += 8
	}
	return output
}

func copyBits(src []byte, srcOffset int, dst []byte, dstOffset, count int) {
	for i := 0; i < count; i++ {
		dst[i+dstOffset] = src[i+srcOffset]
	}
}

func MppcPack(data []byte) []byte {
	i := 0
	num2 := 0
	packedSize := 0
	array2 := byteToBit(data)
	output := make([]byte, len(data)*9+11)
	for i = 0; i < len(array2); i += 8 {
		if array2[i] == 1 {
			output[i+num2] = 1
			num2++
			output[i+num2] = 0
			copyBits(array2, i+1, output, i+num2+1, 7)
		} else {
			copyBits(array2, i, output, i+num2, 8)
		}
	}
	for num3 := 0; num3 < 4; num3++ {
		output[i+num2+num3] = 1
	}
	i += 4
	for num3 := 0; num3 < 6; num3++ {
		output[i+num2+num3] = 0
	}
	i += 6
	if (i+num2)%8 == 0 {
		packedSize = i + num2
	} else {
		packedSize = i + num2 + (8 - ((i + num2) % 8))
	}
	if packedSize >= len(output) {
		return bitToByte(output)
	}
	return bitToByte(output[:packedSize])
}
