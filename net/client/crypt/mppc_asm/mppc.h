#ifndef MPPC_H
#define MPPC_H
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#define MPPC_BUF_SIZE 0x2000
typedef uint8_t byte;
typedef uint32_t dword;
typedef uint64_t qword;

typedef struct {
    qword *hist;
    byte *output;
    byte *output_ptr;
    byte *result;
} mppc_context;

static byte* mppc256_transform(byte *output, int length, byte **src, qword *hist);

mppc_context* create_mppc_context();

void free_mppc_context(mppc_context *this);

static void init_history(mppc_context *this, byte *data);

int mppc_context_pack(mppc_context *this, byte *data, int length);
#endif // MPPC_H
