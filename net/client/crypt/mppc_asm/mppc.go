package mppc_asm

// #include "mppc.h"
import "C"

import (
	"unsafe"
)

type MppcCompress struct {
	context *C.mppc_context
}

func (m *MppcCompress) Close() {
	C.free_mppc_context(m.context)
}

func NewCompress() *MppcCompress {
	m := &MppcCompress{}
	m.context = C.create_mppc_context()
	return m
}
func (m *MppcCompress) Pack(data []byte) []byte {
	var newLenght C.int = 0
	newLenght = C.mppc_context_pack(m.context, (*C.byte)(unsafe.Pointer(&data[0])), C.int(len(data)))
	return C.GoBytes(unsafe.Pointer(m.context.result), C.int(newLenght))
}
