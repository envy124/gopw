#include <stdio.h>
#include "mppc.h"

uint8_t mppc_result[] = { 0x04, 0x19, 0x00, 0x02, 0x65, 0x00, 0x00, 0x00, 0x05, 0x5F, 0x00, 0xF0, 0x40, 0x3E, 0x20, 0x02, 0xFF, 0xE0, 0x80, 0x3C, 0x17, 0x80 };                                                                                              

uint8_t login_announce[] = { 0x04, 0x19, 0x00, 0x02, 0x65, 0x00, 0x00, 0x00, 0x05, 0x5F, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00 };                                                             

void
hexprint(uint8_t *data, int length) {
for (int i = 0; i < length; i++)
    printf("%02hhx", (unsigned)data[i]);
printf("\n");
}

int main(int argc, char *argv[])
{
    mppc_context *mppc = create_mppc_context();
    hexprint(login_announce, sizeof(login_announce));
    int new_length = mppc_context_pack(mppc, login_announce, sizeof(login_announce));
    hexprint(mppc->result, new_length);
    hexprint(mppc_result, sizeof(mppc_result));
    free_mppc_context(mppc);
    return 0;
}