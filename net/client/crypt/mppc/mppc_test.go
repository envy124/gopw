package mppc

import (
	"gopw/ds"
	"gopw/util"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	_ = assert.Equal
	_ = util.LoadSliceFromDir
)

func TestUnpack(t *testing.T) {
	unpacker := NewUnpack()
	compress := NewCompress()
	defer compress.Close()
	defer unpacker.Close()
	mppcPackedOrigin := ds.FromString("0419000000300000001600F0403E2002FFE0803C1780").GetBytes()
	unpackedOrigin := unpacker.Unpack(mppcPackedOrigin)
	packedMppcCWrapper := compress.Pack(unpackedOrigin)
	unpackedMppcCWrapper := unpacker.Unpack(packedMppcCWrapper)
	// t.Logf("%X\n", unpackedOrigin)
	t.Logf("%X\n", packedMppcCWrapper)
	// t.Logf("%X\n", unpackedMppcCWrapper)
	assert.Equal(t, unpackedOrigin, unpackedMppcCWrapper)
}

func TestCPackUnpack(t *testing.T) {
	compress := NewCompress()
	unpacker := NewUnpack()
	defer compress.Close()
	defer unpacker.Close()
	for i, hexstr := range util.LoadSliceFromDir("mppc", "", "S2C") {
		if i%2 == 0 {
			packet := ds.FromString(hexstr).GetBytes()
			mppcPacked := compress.Pack(packet)
			mppcUnpacked := unpacker.Unpack(mppcPacked)
			// t.Logf("%X\n", packet)
			// t.Logf("%X\n", mppcPacked)
			// t.Logf("%X\n", mppcUnpacked)
			assert.Equal(t, packet, mppcUnpacked)
		}
	}
}
