package mppc

/*
#cgo CFLAGS: -O3
#include "mppc.h"
*/
import "C"

import (
	"unsafe"
)

type mppc struct {
	context *C.struct_mppc_context
}

type MppcCompress struct {
	*mppc
}

type MppcUnpack struct {
	*mppc
}

func (m *mppc) Close() {
	C.free_context(m.context)
}

func NewCompress() *MppcCompress {
	m := &MppcCompress{new(mppc)}
	m.context = C.create_compression_context()
	return m
}

func NewUnpack() *MppcUnpack {
	m := &MppcUnpack{new(mppc)}
	m.context = C.create_decompression_context()
	return m
}

func (m *MppcCompress) Pack(data []byte) []byte {
	var newLenght C.u_long = 0
	newLenght = C.pack(m.context, (*C.uint8_t)(unsafe.Pointer(&data[0])), C.u_long(len(data)))
	return C.GoBytes(unsafe.Pointer(m.context.result_ptr), C.int(newLenght))
}

func (m *MppcUnpack) Unpack(data []byte) []byte {
	var newLenght C.u_long = 0
	newLenght = C.unpack(m.context, (*C.uint8_t)(unsafe.Pointer(&data[0])), C.u_long(len(data)))
	unpacked := C.GoBytes(unsafe.Pointer(m.context.result_ptr), C.int(newLenght))
	if len(unpacked) > 0 && data[0] != 0 && unpacked[0] == 0 {
		return unpacked[1:]
	}
	return unpacked
}
