#include "mppc.h"

static struct mppc_context*
create_context_stub() {
	struct mppc_context* mppc = malloc(sizeof(struct mppc_context));
	mppc->buffer = (u_char *)malloc(BUFFER_LENGTH);
	mppc->buffer_ptr = mppc->buffer;
	mppc->buffer_len = malloc(sizeof(u_long));
	*mppc->buffer_len = BUFFER_LENGTH;
	return mppc;
}

struct mppc_context*
create_compression_context() {
	struct mppc_context* mppc = create_context_stub();
	mppc->history = (u_char *)malloc(MPPC_SizeOfCompressionHistory());
	MPPC_InitCompressionHistory(mppc->history);
	return mppc;
}

struct mppc_context*
create_decompression_context() {
	struct mppc_context* mppc = create_context_stub();
	mppc->history = (u_char *)malloc(MPPC_SizeOfDecompressionHistory());
	MPPC_InitDecompressionHistory(mppc->history);
	return mppc;
}

void
free_context(struct mppc_context* mppc) {
	if (mppc) {
	if (mppc->buffer) free(mppc->buffer);
	if (mppc->history) free(mppc->history);
	if (mppc->buffer_len) free(mppc->buffer_len);
	free(mppc);
	}
}

void
hexprint(uint8_t *data, int length) {
for (int i = 0; i < length; i++)
	printf("%02hhx", (unsigned)data[i]);
printf("\n");
}

u_long
pack(struct mppc_context* mppc, uint8_t* data, u_long length) {
	u_char **src = &data;
	u_char **dst = &mppc->buffer_ptr;
	mppc->result_ptr = mppc->buffer_ptr;
	u_long old_buffer_len = *mppc->buffer_len;
	int flag = MPPC_Compress(src, dst, &length, mppc->buffer_len, mppc->history, MPPC_SAVE_HISTORY);
	// fprintf(stderr, "MPPC pack result flag %d\n", flag);
	return old_buffer_len - *mppc->buffer_len;
}

u_long
unpack(struct mppc_context* mppc, uint8_t* data, u_long length) {
	// printf("Unpack called with:\n");
	// hexprint(data, length);
	u_char **src = &data;
	u_char **dst = &mppc->buffer_ptr;
	mppc->result_ptr = mppc->buffer_ptr;
	u_long old_buffer_len = *mppc->buffer_len;
	// printf("srcCnt %x dstCnt %x \n", length, *mppc->buffer_len);
	int flag = MPPC_Decompress(src, dst, &length, mppc->buffer_len, mppc->history, MPPC_SAVE_HISTORY);
	// printf("MPPC unpack result flag %d\n", flag);
	// printf("srcCnt %x dstCnt %x \n", length, *mppc->buffer_len);
	// hexprint(mppc->result_ptr, old_buffer_len - *mppc->buffer_len);
	return old_buffer_len - *mppc->buffer_len;
}