package crypt

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gopw/ds"
	"gopw/util"
	"testing"
)

var data = util.LoadFromDir("mppc", "", "S2C")

func TestUnpack(t *testing.T) {
	var isPacked bool
	mppc := &MppcUnpacker{}
	var testPackets []string
	for hexstr := range data {
		testPackets = append(testPackets, hexstr)
	}
	for i, hexstr := range testPackets {
		if !isPacked {
			packed := ds.FromString(hexstr).GetBytes()
			unpacked := mppc.Unpack(packed)
			assert.Equal(t, testPackets[i+1], fmt.Sprintf("%X", unpacked))
		}
		isPacked = !isPacked
	}
}
