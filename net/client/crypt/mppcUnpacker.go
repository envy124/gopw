package crypt

type MppcUnpacker struct {
	code1, code2, code3, code4 uint32
	offset                     uint32
	packed, unpacked, chunk    []byte
}

func (m *MppcUnpacker) Close() {

}

func (m *MppcUnpacker) UnpackByte(x byte) []byte {
	m.packed = append(m.packed, x)
	m.chunk = nil
	m.chunk = make([]byte, 0)
	if len(m.unpacked) > 10240 {
		m.unpacked = m.unpacked[2048:]
	}
	for {
		if m.code3 == 0 {
			if m.hasBits(4) {
				if m.getPacketBits(1) == 0 {
					// 0-xxxxxxx
					m.code1 = 1
					m.code3 = 1
				} else {
					if m.getPacketBits(1) == 0 {
						// 10-xxxxxxx
						m.code1 = 2
						m.code3 = 1
					} else {
						if m.getPacketBits(1) == 0 {
							// 110-xxxxxxxxxxxxx-*
							m.code1 = 3
							m.code3 = 1
						} else {
							if m.getPacketBits(1) == 0 {
								// 1110-xxxxxxxx-*
								m.code1 = 4
								m.code3 = 1
							} else {
								// 1111-xxxxxx-*
								m.code1 = 5
								m.code3 = 1
							}
						}
					}
				}
			} else {
				break
			}
		} else if m.code3 == 1 {
			if m.code1 == 1 {
				if m.hasBits(7) {
					b := byte(m.getPacketBits(7))
					m.chunk = append(m.chunk, b)
					m.unpacked = append(m.unpacked, b)
					m.code3 = 0
				} else {
					break
				}
			} else if m.code1 == 2 {
				if m.hasBits(7) {
					b := byte(m.getPacketBits(7) | 0x80)
					m.chunk = append(m.chunk, b)
					m.unpacked = append(m.unpacked, b)
					m.code3 = 0
				} else {
					break
				}
			} else if m.code1 == 3 {
				if m.hasBits(13) {
					m.code4 = m.getPacketBits(13) + 0x140
					m.code3 = 2
				} else {
					break
				}
			} else if m.code1 == 4 {
				if m.hasBits(8) {
					m.code4 = m.getPacketBits(8) + 0x40
					m.code3 = 2
				} else {
					break
				}
			} else if m.code1 == 5 {
				if m.hasBits(6) {
					m.code4 = m.getPacketBits(6)
					m.code3 = 2
				} else {
					break
				}
			}
		} else if m.code3 == 2 {
			if m.code4 == 0 {
				// Guess !!!
				if m.offset != 0 {
					m.offset = 0
					m.packed = m.packed[1:]
				}
				m.code3 = 0
				continue
			}
			m.code2 = 0
			m.code3 = 3
		} else if m.code3 == 3 {
			if m.hasBits(1) {
				if m.getPacketBits(1) == 0 {
					m.code3 = 4
				} else {
					m.code2++
				}
			} else {
				break
			}
		} else if m.code3 == 4 {
			var copySize uint32

			if m.code2 == 0 {
				copySize = 3
			} else {
				size := uint32(m.code2 + 1)

				if m.hasBits(size) {
					copySize = m.getPacketBits(size) + (1 << size)
				} else {
					break
				}
			}

			m.copy(m.code4, copySize)
			m.code3 = 0
		}
	}
	return m.chunk
}

func (m *MppcUnpacker) Unpack(bytes []byte) []byte {
	output := make([]byte, 0)
	for _, bt := range bytes {
		output = append(output, m.UnpackByte(bt)...)
	}
	return output
}

func (m *MppcUnpacker) copy(shift, size uint32) {
	for i := 0; i < int(size); i++ {
		pindex := uint32(len(m.unpacked)) - shift
		if pindex < 0 {
			return
		}
		b := m.unpacked[pindex]
		m.unpacked = append(m.unpacked, b)
		m.chunk = append(m.chunk, b)
	}
}

func (m *MppcUnpacker) getPacketBits(bitCount uint32) uint32 {
	if bitCount > 16 {
		return 0
	}
	if !m.hasBits(bitCount) {
		panic("MppcUnpack bit stream overflow")
	}
	alBitCount := bitCount + m.offset
	alByteCount := (alBitCount + 7) / 8
	var v, i uint32
	for i = 0; i < uint32(alByteCount); i++ {
		v |= uint32(m.packed[i]) << (24 - i*8)
	}
	v <<= uint32(m.offset)
	v >>= uint32(32 - bitCount)
	m.offset += bitCount
	freeBytes := m.offset / 8
	if freeBytes != 0 {
		m.packed = m.packed[freeBytes:]
	}
	m.offset %= 8
	return v
}

func (m *MppcUnpacker) hasBits(count uint32) bool {
	return (uint32(len(m.packed))*8 - m.offset) >= count
}
