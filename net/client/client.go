package client

import (
	"gopw/ds"
	"gopw/net/client/crypt"
	"gopw/net/packet"
	"gopw/util"
	"gotools/netHelp"
	"net"
	"time"
)

const timeOut = 20 * time.Second
const bufferLength = 0x2000

var DroppedClients chan *Client

type Client struct {
	encdec               *crypt.EncDec
	login                string
	socket               net.Conn
	isWork               bool
	sendQueue, recvQueue chan *packet.Packet
}

func deadline() time.Time {
	return time.Now().Add(timeOut)
}

func NewClient(login string) *Client {
	c := new(Client)
	c.login = login
	c.sendQueue = make(chan *packet.Packet)
	c.recvQueue = make(chan *packet.Packet)
	c.encdec = new(crypt.EncDec)
	return c
}

func (c *Client) Login() string {
	return c.login
}

func (c *Client) EncDec() *crypt.EncDec {
	return c.encdec
}

func (c *Client) IsWork() bool {
	return c.isWork
}

func (c *Client) String() string {
	return c.login
}

func (c *Client) Close() {
	if c.isWork {
		c.isWork = false
		c.socket.Close()
		close(c.sendQueue)
		close(c.recvQueue)
		if DroppedClients != nil {
			DroppedClients <- c
		}
	}
}

func (c *Client) Connect(addr string) {
	var err error
	c.socket, err = netHelp.Connect(addr)
	util.PanicIf(err)
	c.isWork = true
	c.startRecvManager()
	c.startSendManager()
}

func (c *Client) startRecvManager() {
	go func() {
		buffer := make([]byte, bufferLength)
		stream := new(ds.DataStream)
		for c.isWork {
			c.socket.SetReadDeadline(deadline())
			count, err := c.socket.Read(buffer)
			if err != nil {
				c.Close()
				return
			}
			stream.WriteBytes(c.encdec.Decode(buffer[:count]))
			for p := range packet.GetPackets(stream) {
				c.recvQueue <- p
			}
		}
	}()
}

func (c *Client) startSendManager() {
	go func() {
		for p := range c.sendQueue {
			if !c.isWork {
				return
			}
			c.socket.SetWriteDeadline(deadline())
			_, err := c.socket.Write(c.encdec.Encode(p))
			if err != nil {
				c.Close()
				return
			}
		}
	}()
}

func (c *Client) Send(p *packet.Packet) {
	c.sendQueue <- p
}

func (c *Client) Recv() *packet.Packet {
	return <-c.recvQueue
}
